// MARK: Задание 1:
// Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную.
let numbers = [7, 3, 6, 5, 2, 4, 8, 1, 9]

let sortedNumbersAscending  = numbers.sorted(by: { $0 < $1 })
let sortedNumbersDescending = numbers.sorted(by: { $0 > $1 })


// MARK: Задание 2:
// Вывести результат в консоль.
print(sortedNumbersAscending)
print(sortedNumbersDescending)


// MARK: Задание 3:
// Создать метод, который принимает имена друзей, после этого имена положить в массив.
var names = ["Сережа", "Егор", "Соня"]

func addFriends(newNames: String...) -> [String] {
    for name in newNames {
        names.append(name)
    }
    
    return names
}

let myFriends = addFriends(newNames: "Марат", "Владимир", "Оля")
print(myFriends)


// MARK: Задание 4:
// Массив отсортировать по количеству букв в имени.
let sortedNames = names.sorted(by: { $0.count < $1.count })
print(sortedNames)


// MARK: Задание 5:
// Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга.
var namesByLength = [Int: [String]]()

for name in names {
    let length = name.count
    if var names = namesByLength[length] {
        names.append(name)
        namesByLength[length] = names
    } else {
        namesByLength[length] = [name]
    }
}

print(namesByLength)


// MARK: Задание 6:
// Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение.
func printValueForKey(_ key: Int) {
    if let value = namesByLength[key] {
        print("Имена с \(key) буквами: \(value).")
    } else {
        print("Не найдено имен количеством букв равным \(key).")
    }
}

printValueForKey(4)


// MARK: Задание 7:
// Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль.
func checkArraysAndPrint(stringArray: [String], numberArray: [Int]) {
    var stringArrayCopy = stringArray
    var numberArrayCopy = numberArray
    
    if stringArrayCopy.isEmpty {
        print("Массив пуст, добавляю значение.")
        stringArrayCopy.append("default")
    }
    
    if numberArrayCopy.isEmpty {
        print("Массив пуст, добавляю значение.")
        numberArrayCopy.append(0)
    }
    
    print("Массив строк: \(stringArrayCopy)")
    print("Массив номеров: \(numberArrayCopy)")
}

let emptyStringArray: [String] = []
let emptyNumberArray: [Int]    = []

let stringArray: [String] = ["Ейск", "Екатеринбург", "Москва", "Чебоксары"]
let numberArray: [Int]    = [1, 3, 9, 7, 5, 2]

checkArraysAndPrint(stringArray: emptyStringArray, numberArray: emptyNumberArray)
checkArraysAndPrint(stringArray: stringArray, numberArray: numberArray)


